require 'rest-client'
require 'csv'

def populate_sih()
    puts 'Saving sih data.'
    counter = 0
    sih_csv_path = File.join(__dir__, "csv/procedures.csv")
    CSV.foreach(sih_csv_path, headers: true) do |row|
        values = {}
        values[:properties] = {}
        values[:properties][:database] = "SIH"
        values[:properties][:distance] = row[36].to_f
        # values[:information] = {}
        values[:properties][:CD_GEOCODI] = row[0]

        point = {}
        point[:type] = "Point"
        point[:coordinates] = [row[1].to_f, row[2].to_f]

        for i in 3..35
            values[:properties][row.headers[i]] = row[i]
        end

        r = Register.new(values)
        r.loc = Loc.new(point)
        r.save
        counter += 1

        progress = ((counter.to_f / 554202.0) * 100.0)
        printf("\rProgress: [%-100s] %d%%", "=" * progress, progress)
    end

    puts ""
    puts "#{counter} rows successfully saved"
end

populate_sih()
