require 'rails_helper'

RSpec.describe RegistersController, type: :controller do

  describe "GET #index" do
    before :each do
      r = Register.create(_id: BSON::ObjectId('59afe79b92caf8948b000005'), properties: {"CNES" => 10298, "IDADE" => 20})
      Loc.create(register: r, type: "Point", coordinates: [-46.5433, -23.4312])
    end

    after :each do
      Mongoid.purge!
    end

    it "returns http success" do
      id = '59afe79b92caf8948b000005'
      self.send(:get, 'index', params: {id: id}, format: :json)
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #create" do
    after :each do
      Mongoid.purge!
    end

    it "returns http success" do
      parameters = {}
      parameters["type"] = "Feature"
      parameters["geometry"] = {"type" => "Point", "coordinates" => [-46.12345, -23.5444]}
      parameters["properties"] = {"CNES" => 112200, "ESPECIALID" => 1, "CNES_DENO" => "HOSP CLINICAS"}
      self.send(:post, 'create', params: parameters, format: :json)
      expect(response).to have_http_status(:success)
      parsed = JSON.parse response.body
      expect(parsed["id"]).to eq(Register.first.id.to_s)
    end
  end

  # describe "GET #update" do
  #   it "returns http success" do
  #     get :update
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "GET #destroy" do
  #   it "returns http success" do
  #     get :destroy
  #     expect(response).to have_http_status(:success)
  #   end
  # end

end
