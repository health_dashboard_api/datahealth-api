require 'json'
require 'csv'

def getPop
    pop_csv = File.join(__dir__, "Setor_SP.csv")
    sec_counter = 0
    pop_sectors = {}
    CSV.foreach(pop_csv, :headers => true) do |row|
        pop_sectors[row[0]] = row[33]
        sec_counter += 1
    end
    puts "#{sec_counter} setores"
    return pop_sectors
end

def create_geojson
    geojson = {}
    geojson[:type] = "FeatureCollection"
    features = []
    Sector.each do  |register|
        feature = {}
        feature[:type] = "Feature"
        geometry = register[:loc]
        geometry.delete("_id")
        feature[:geometry] = geometry
        feature[:properties] = register.properties
        features.append(feature)
    end
    geojson[:features] = features

    puts('Save geojson file with results')
    fJson = File.open(Rails.root.join("public/Setor_with_pop.geojson"), "w")
    fJson.write(geojson.to_json)
    fJson.close()
end

def populate_sectors()
    counter = 0
    files = ["SetorCensitario", "DA", "STS", "SP", "CRS", "ESF", "PR", "UBS"]


    files.each do |name|
        puts "Saving #{name} data."
        file = File.read(__dir__ + "/Shape_#{name}.geojson")
        geojson = JSON.parse(file)
        total = geojson["features"].count.to_f
        geojson["features"].each do |feature|
            values = {}

            values[:type] = feature["geometry"]["type"]
            values[:coordinates] = feature["geometry"]["coordinates"]
            sector = Sector.new()
            sector.loc = Loc.new(values)
            sector.properties = feature["properties"]
            sector[:database] = name
            sector.save
            
            counter += 1

            progress = ((counter.to_f / total) * 100.0)
            printf("\rProgress: [%-100s] %d%%", "=" * progress, progress)
        end
        puts ""
        puts "#{counter} #{name} successfully saved"
        puts ""
        counter = 0
    end
end

populate_sectors()
# create_geojson()
