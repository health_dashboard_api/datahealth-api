require 'json'
require 'csv'
require 'restclient'

def populate_hc()
    counter = 0

    puts "Saving HC data."
	hc = File.join(__dir__, "health_centres_real.csv")

	CSV.foreach(hc, :headers => true) do |row|
		geojson = {}
		geojson["type"] = "Feature"
		geojson["properties"] = {}
		geojson["properties"]["CNES"] = row[0]
		geojson["properties"]["CNES_DENO"] = row[1]
		geojson["properties"]["CNES_DENOR"] = row[2]
		geojson["properties"]["LEITOS"] = row[3]
		geojson["properties"]["TIPO"] = row[6]
		geojson["properties"]["REGIONAL"] = row[7]
		geojson["properties"]["FONE"] = row[8]
		geojson["properties"]["GESTAO"] = row[9]
		geojson["properties"]["DA"] = row[10]
		geojson["properties"]["PR"] = row[11]
		geojson["properties"]["STS"] = row[12]
		geojson["properties"]["CRS"] = row[13]
		geojson["geometry"] = {}
		geojson["geometry"]["type"] = "Point"
		geojson["geometry"]["coordinates"] = [row[5].to_f, row[4].to_f]
		geojson["properties"]["database"] = "HC"

		puts geojson
		puts RestClient.post("http://localhost:3000/register", geojson, headers={})
	end
end

populate_hc()