class ApiController < ApplicationController
    before_action :filterRegisters, only: [:query, :group, :clusterRegisters]

    # POST /api/query
    def query
        result = Register.collection.aggregate(@filters)
        response = to_geojson("query", result)
        render json: response, status: 200 and return
    end

    # POST /api/query
    def group
        result = Register.collection.aggregate(@filters)
        response = to_geojson("group", result)
        render json: response, status: 200 and return
    end

    # POST /api/geolocation
    def geolocation
        parameters = filters_params()
        sectors = Sector.where(database: parameters[:database])
        if parameters[:type] == "coordinates"
            result = sectors.geo_spacial(:loc.intersects_point => parameters[:location])
        elsif parameters[:type] == "address"
            # TODO: get an address geolocated it and find the polygon

            # Simple way
            geocode = Geocoder.search(parameters[:location]).first
            coordinates = [geocode.data["lon"].to_f, geocode.data["lat"].to_f]
            result = sectors.geo_spacial(:loc.intersects_point => coordinates)
            #
        end

        response = to_geojson("geolocation", result)
        render json: response, status: 200 and return
    end

    private

    def to_geojson(type, points)
        geojson = {}
        geojson[:type] = "FeatureCollection"
        features = []
        points.each do  |point|
            feature = {}
            feature[:type] = "Feature"

            #geolocation
            if type == "geolocation"
                feature[:geometry] = {type: point[:loc][:type], coordinates: point[:loc][:coordinates]}
                feature[:properties] = point[:properties]
            end

            # query
            if type == "query"
                feature[:geometry] = {type: point[:loc][:type], coordinates: point[:loc][:coordinates]}
                feature[:properties] = point[:properties]
            end

            # group
            if type == "group"
                if point["_id"] == nil
                    feature[:properties] = {:result => point[:result]}
                elsif point["_id"][:loc] != nil
                    feature[:geometry] = {:type => "Point", :coordinates => point["_id"][:loc].to_a}
                    feature[:properties] = {:counter => point[:counter]}
                else
                    feature[:properties] = {point["_id"][:field] => point[:counter]}
                end
            end
            features.append(feature)
        end
        geojson[:features] = features
        return geojson.to_json
    end

    def filterRegisters
        parameters = filters_params()
        @filters = []
        if parameters[:properties] != nil
            parameters[:properties].keys.each do |key|
                if valid_op(key)
                    if key == "location"
                        @filters.prepend({"$geoNear" => {near: {type: "Point",
                            coordinates: [parameters[:properties][key][0], parameters[:properties][key][1]]},
                            distanceField: "dist.calculated", includeLocs: "dist.location",
                            maxDistance: 1}})
                    else
                        parameters[:properties][key].each do |values|
                            clause = "properties." + values[0]
                            @filters.append({"$match".to_sym =>
                                {clause.to_sym => {"$#{key}".to_sym => values[1]}}})
                        end
                    end
                end
            end
        end

        if parameters[:group] != nil
            parameters[:group].keys.each do |key|
                if valid_group(key)
                    if key == "count"
                        parameters[:group][key].each do |value|
                            if value == "coordinates"
                                @filters.append({"$group" => {_id: {loc: "$loc.coordinates"},
                                    counter: {"$sum" => 1}}})
                            else
                                @filters.append({"$group" => {_id: {field: "$properties.#{value}"},
                                    counter: {"$sum" => 1}}})
                            end
                        end
                    else
                        parameters[:group][key].each do |value|
                            @filters.append({"$group" => {_id: nil,
                                result: {"$#{key}" => "$properties.#{value}"}}})
                        end
                    end
                end
            end
        end
    end

    def valid_group (group)
        return false unless ["count", "max", "avg", "min", "sum"].include? group
        return true
    end

    def valid_op (op)
        return false unless ["eq", "gt", "gte", "lt", "lte", "in", "ne", "location"].include? op
        return true
    end

    def filters_params
        parsedJSON = JSON.parse params["data"].to_s
        load_params = ActionController::Parameters.new
        load_params[:location] = parsedJSON["location"]
        load_params[:properties] = parsedJSON["properties"]
        load_params[:group] = parsedJSON["group"]
        load_params[:database] = parsedJSON["database"]
        load_params[:type] = parsedJSON["type"]
        load_params.permit!
        return load_params
    end
end
