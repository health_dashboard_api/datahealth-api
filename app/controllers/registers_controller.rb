class RegistersController < ApplicationController
    def index
        @register = Register.find(params[:id])
        render json: @register, status: 200 and return
    end

    def create
        parameters = paramsParser()
        id = nil
        if (parameters[:type] == "FeatureCollection")
            id = registerCollection(parameters[:features])
        elsif (parameters[:type] == "Feature")
            id = registerPoint(parameters)
        end

        response = {}
        response[:message] = "Success"
        response[:id] = id
        render json: response.to_json, status: 200 and return
    end

    def update
        @register = Register.find(params[:id])

        if @register.update_attributes(paramsParser)
            response = {}
            response[:message] = "Success"
            response[:id] = @register._id.to_s
            render json: response.to_json, status: 200 and return
        end
    end

    def destroy
        @register = Register.find(params[:id])
        if @register.destroy
            response = {}
            response[:message] = "Success"
            render json: response.to_json, status: 200 and return
        end
    end

    private

    def registerPoint(parameters)
        values = {}
        values[:type] = parameters[:geometry][:type]
        values[:coordinates] = [parameters[:geometry][:coordinates][0].to_f, parameters[:geometry][:coordinates][1].to_f]
        register = Register.new()
        register.loc = Loc.new(values)
        register.properties = parameters[:properties].to_h
        register.save

        return register.id.to_s
    end

    def registerCollection(features)
        ids = []
        features.each do |feature|
            ids.append(registerPoint(feature))
        end

        return ids
    end

    def paramsParser
        load_params = ActionController::Parameters.new
        load_params[:type] = params["type"]
        load_params[:features] = params["features"]
        load_params[:geometry] = params["geometry"]
        load_params[:properties] = params["properties"]
        load_params.permit!

        return load_params
    end
end
