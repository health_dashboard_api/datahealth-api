class Sector
    include Mongoid::Document
    include Mongoid::Attributes::Dynamic

    field :properties, type: Hash
    field :database, type: String
    embeds_one :loc

    def self.find_polygons_with_point(coordinates)
    	Sector.all.geo_spacial(:loc.intersects_point => coordinates)
    end
end
