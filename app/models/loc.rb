class Loc
    include Mongoid::Document

    field :type, type: String
    field :coordinates, type: Array

    embedded_in :sector
    embedded_in :register
end