class Register
    include Mongoid::Document
    include Mongoid::Attributes::Dynamic

    field :properties, type: Hash
    embeds_one :loc

    def to_s
        "_id: #{self.id} location: #{self.loc} properties: #{self.properties}"
    end
    index("loc.coordinates": "2dsphere")
end
