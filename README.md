# API

[![pipeline status](https://gitlab.com/health_dashboard_api/datahealth-api/badges/master/pipeline.svg)](https://gitlab.com/health_dashboard_api/datahealth-api/commits/master)
[![coverage report](https://gitlab.com/health_dashboard_api/datahealth-api/badges/master/coverage.svg)](https://gitlab.com/health_dashboard_api/datahealth-api/commits/master)

API for data related to SIH, SIM and SINASC databases

### Using:

* MongoDB server version 4.0.2
* Rails >= 5.0.1
